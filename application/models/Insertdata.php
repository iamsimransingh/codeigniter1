<?php
class Insertdata extends CI_Model{
  function registeruser($data){
    if($this->db->insert('users',$data)){
      return true;
    }else{
      return false;
    }
  }
  function get_users(){

    $result = $this->db->get('users');
    return $result->result_array();
  }
  function check_username_fromdb($username){
    $this->db->where('username',$username);
    $res = $this->db->get('users');
    return $res->num_rows();

  }
}
?>
