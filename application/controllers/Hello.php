<?php
// localhost/code/index.php/controller_name/method_name
class Hello extends CI_Controller {
  public function index(){
    echo "Hello Code Igniter";
  }
  public function login(){
    echo "Login function";
  }
  public function htmlview(){
    $this->load->view('header');
    $this->load->view('hello');
    $this->load->view('footer');
  }
  public function register(){
    $this->load->view('header');
    $this->load->view('register');
    $this->load->view('footer');
  }
  public function registerme(){
    $fullname = $this->input->post('fullname');
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $about = $this->input->post('about');
    $arr = array(
      'fullname' => $fullname,
      'username' => $username,
      'password' => $password,
      'about' => $about
    );
    $this->load->model('insertdata');
    if($this->insertdata->registeruser($arr)){
      echo "Insert";
    }else{
      echo "Error";
    }

  }
  // Register me end
  public function showdata(){
    $this->load->model('insertdata');

    $data['hello'] = $this->insertdata->get_users();
    $this->load->view('header');
    $this->load->view('showdata',$data);
    $this->load->view('footer');
  }
  public function check_username(){
    $username = $this->input->post('username');
    $this->load->model('insertdata');
    $num = $this->insertdata->check_username_fromdb($username);
    $arr = array('count' => $num);
    echo json_encode($arr);
    header('Content-Type: application/json');

  }
}

?>
