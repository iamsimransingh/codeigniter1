<script>
  function check_username(){
    var username = $('#username').val();
    $.ajax({
      url: "<?php echo base_url(); ?>index.php/hello/check_username",
      type: 'post',
      data: {username:username},
      success: function(data){
        if(data.count > 0){
          $('#output').html('Username Already Exist');
          $('#submit').attr('disabled','disabled');
          
        }else{
          $('#output').html('Username Available');
          $('#submit').removeAttr('disabled');
        }
        //alert(JSON.stringify(data));
      }
    });
  }
</script>
</body>
</html>
