<div class="container">
  <div class="row">
    <div class="col-md-6 mx-auto mt-5 bg-info p-5">
      <div class="form-group">
        <?php echo form_open('hello/registerme'); ?>
        <label>Full Name</label>
        <input type="text" name="fullname" class="form-control" />
        <label>Username</label>
        <input type="text" name="username" id="username" class="form-control" onchange="check_username();" />
        <div id="output"></div>
        <label>Password</label>
        <input type="password" name="password" class="form-control" />
        <label>About You</label>
        <textarea class="form-control" name="about"></textarea>
        <input type="submit" id="submit" class="btn btn-success mt-3" />
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div>
